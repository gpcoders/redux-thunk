import React, {Component} from 'react';
import { connect } from 'react-redux'
import { createPost } from '../actions/postAction'

class Postform extends Component {

    state = {
        title: "",
        body: ""
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit(e) {
        e.preventDefault()

        const post = {
            title: this.state.title,
            body: this.state.body
        }

        // action will be call here
        this.props.createPost(post)
    }

    render() {
        return (
            <div>
                <h1>Add Post</h1>
                <form onSubmit={this.onSubmit.bind(this)}>
                    <div>
                        <label>Title</label> <br />
                        <input type="text" name="title" value={this.state.title} onChange={(e) =>  this.onChange(e)}/>
                    </div>
                    <div>
                        <label>Body</label> <br />
                        <textarea name="body" value={this.state.body} onChange={(e) =>  this.onChange(e)}></textarea>
                    </div>
                    <br/>
                    <button type="submit">Submit</button>
                </form>
            </div>
        );
    }
}

export default connect(null, { createPost })(Postform);
