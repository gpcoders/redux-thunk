import { FETCH_POSTS, NEW_POST } from '../actions/types'

const initState = {
    items: [],
    item: {},
    isLoading: false
}

export default function (state = initState, action) {
    switch (action.type) {
        case FETCH_POSTS:
            console.log(action)
            return {
                isLoading: true,
                ...state,
                items: action.payload,
                isLoading: false
            }
        case NEW_POST:
            return {
                ...state,
                item: action.payload
            }
        default:
            return state
    }
}

