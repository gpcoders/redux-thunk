import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk';

import rootReducer from './reducers/rootReducer';

const initialState = {};
const middlewrae = [thunk]

const store = createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middlewrae), window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()),
);

export default store;